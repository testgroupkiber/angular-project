import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContadorComponent } from './contador.component';

describe('ContadorComponent', () => {
  let component: ContadorComponent;
  let fixture: ComponentFixture<ContadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      // Aquí ya no incluyes a ContadorComponent en declarations
      // imports: [ ContadorComponent ] // Descomenta esto solo si es necesario
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('debe incrementar el contador', () => {
    component.incrementar();
    expect(component.contador).toBe(1);
  });

  it('debe decrementar el contador', () => {
    component.contador = 1;
    component.decrementar();
    expect(component.contador).toBe(0);
  });
});
