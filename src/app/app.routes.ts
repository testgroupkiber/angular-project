import { Routes } from '@angular/router';
import { ContadorComponent } from './contador/contador.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', component: ContadorComponent}
];
