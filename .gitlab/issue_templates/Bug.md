**Título del Issue**: [Breve descripción del problema]

**Resumen del Issue**
- Breve resumen del problema, incluyendo contexto relevante y cualquier impacto conocido.

**Pasos para Reproducir**
1. [Primer paso]
2. [Segundo paso]
3. [Siguientes pasos si son necesarios]
- Incluir cualquier configuración o entorno relevante.

**Comportamiento Actual**
- Descripción detallada del comportamiento actual del sistema, incluyendo capturas de pantalla o mensajes de error, si es aplicable.

**Comportamiento Esperado**
- Descripción clara y detallada de lo que se espera que suceda bajo las mismas circunstancias.

**Información Adicional (Opcional)**
- Cualquier otro detalle que pueda ser útil, como posibles soluciones o sugerencias.

**Etiquetas (Opcional)**
- [Agregar etiquetas relevantes para clasificar y priorizar el issue]

